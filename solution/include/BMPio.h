#ifndef ASSIGNMENT_3_IMAGE_ROTATION_BMPIO_H
#define ASSIGNMENT_3_IMAGE_ROTATION_BMPIO_H

#include "image.h"
#include <stdio.h>

#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_FILE_OPEN_ERROR,
    READ_OUT_OF_MEMORY
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_FILE_OPEN_ERROR,
    WRITE_HEADER_FAILED
};

enum read_status read_bmp(const char* filename, struct image* im);
enum write_status write_bmp(const char* filename, struct image const* img);

#endif
