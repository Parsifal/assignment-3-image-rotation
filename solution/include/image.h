#ifndef ASSIGNMENT_3_IMAGE_ROTATION_STRUCTURE_H
#define ASSIGNMENT_3_IMAGE_ROTATION_STRUCTURE_H


#include <inttypes.h>
#include <stdbool.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct pixel {
    uint8_t b, g, r;
};
#pragma pack(pop)

struct image {
    uint64_t width, height;
    struct pixel *data;
};

bool create_image(struct image *im, const uint64_t width, const uint64_t height);

void free_image(struct image *image);

#endif
