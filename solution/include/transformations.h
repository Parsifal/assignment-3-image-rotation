//
// Created by danil on 19.11.2023.
//

#ifndef ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORMATIONS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_TRANSFORMATIONS_H

#include "image.h"
enum rotation_status {
    ROTATION_OK,
    ROTATION_OUT_OF_MEMORY
};

enum rotation_status rotate(struct image* source, const int64_t angle);

#endif
