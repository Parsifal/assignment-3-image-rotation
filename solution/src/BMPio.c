#include "BMPio.h"

static uint16_t get_padding(uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    struct bmp_header header;
    if (fread(&header, sizeof(struct bmp_header), 1, in) != 1)
        return READ_INVALID_HEADER;
    if (header.bfType != 0x4D42 || header.biBitCount != 24 || header.biCompression != 0)
        return READ_INVALID_SIGNATURE;
    if (!create_image(img, header.biWidth, header.biHeight)) return READ_OUT_OF_MEMORY;
    uint16_t padding = get_padding(img->width);
    for (uint64_t y = 0; y < img->height; y++) {
        if (fread(&img->data[y * img->width], sizeof(struct pixel), img->width, in) != img->width) {
            free_image(img);
            return READ_INVALID_BITS;
        }
        for (uint16_t i = 0; i < padding; i++)
            fgetc(in);
    }
    return READ_OK;
}

struct bmp_header create_header(const uint64_t width, const uint64_t height){
    uint16_t padding = get_padding(width);
    struct bmp_header header = {
        .bfType = 0x4D42,     // "BM" в шестнадцатеричном формате
        .bfileSize = (uint32_t)((padding + width * sizeof(struct pixel)) * height + sizeof(struct bmp_header)), // размер заголовка плюс размер массива пикселей
        .bOffBits = sizeof(struct bmp_header), // смещение, где начинаются данные пикселей
        .biSize = 40, // размер структуры BITMAPINFOHEADER, должен быть 40
        .biWidth = (uint32_t)width, // ширина изображения в пикселях
        .biHeight = (uint32_t)height, // высота изображения в пикселях
        .biPlanes = 1, // количество плоскостей, должно быть 1
        .biBitCount = 24, // количество бит на пиксель, 24 для 24-битного изображения
        .biSizeImage = (uint32_t)((padding + width * sizeof(struct pixel)) * height) // размер массива пикселей
    };
    return header;
}

enum write_status to_bmp(FILE *out, struct image const *img) {
    uint16_t padding = get_padding(img->width);
    struct bmp_header header = create_header(img->width, img->height);
    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_HEADER_FAILED;
    }
    for (uint64_t i = 0; i < img->height; i++)
        if (fwrite(img->data + (i * img->width), sizeof(struct pixel), img->width, out) != img->width || fwrite("\0\0\0", padding, 1, out) != 1) 
            return WRITE_ERROR;
    return WRITE_OK;
}

// Открывает файл с заданным именем и режимом и возвращает указатель на него
// В случае ошибки возвращает NULL и выводит сообщение об ошибке
enum read_status read_bmp(const char *filename, struct image* im) {
    FILE *file = fopen(filename, "rb");
    if (!file) return READ_FILE_OPEN_ERROR;
    enum read_status status = from_bmp(file, im);
    fclose(file);
    return status;
}

enum write_status write_bmp(const char* filename, struct image const* img) {
    FILE* file = fopen(filename, "wb");
    if (!file) return WRITE_FILE_OPEN_ERROR;
    enum write_status status = to_bmp(file, img);
    fclose(file);
    return status;
}
