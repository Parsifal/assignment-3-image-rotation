#include "image.h"

struct image new_image(void) {
    return (struct image) {0};
}

bool create_image(struct image *im, const uint64_t width, const uint64_t height) {
    if (width == 0 || height == 0) return false;
    struct pixel* data = malloc(sizeof(struct pixel) * width * height);
    if (!data) return false;
    *im = (struct image) { width, height, data };
    return true;
}

void free_image(struct image *image) {
    free(image->data);
    image->data = NULL;
}
