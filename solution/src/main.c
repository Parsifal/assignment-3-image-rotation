#include "transformations.h"
#include "BMPio.h"

#define NUMBER_OF_ARGV 4

static int error(const char* message) {
    fprintf(stderr, "%s\n", message);
    return 1;
}

int main(int argc, char **argv) {
    if (argc != NUMBER_OF_ARGV) return error("Incorrect number of arguments. Expected 3 arguments.");
    char* input_file_path = argv[1];
    char* output_file_path = argv[2];
    char* invalid_angle;
    long angle = strtol(argv[3], &invalid_angle, 10);
    if (*invalid_angle || angle % 90 || labs(angle) > 270)
        return error("Incorrect angle. Possible values: 0, 90, -90, 180, -180, 270, -270");
    struct image source_image;
    switch (read_bmp(input_file_path, &source_image)) {
    case (READ_INVALID_HEADER):
        return error("Invalid header error.");
    case (READ_INVALID_SIGNATURE):
        return error("Invalid signature error.");
    case (READ_OUT_OF_MEMORY):
        return error("Error read out of memory.");
    case (READ_INVALID_BITS):
        return error("Invalid bits error.");
    case (READ_FILE_OPEN_ERROR):
        return error("Error open file.");
    case (READ_OK):
        break;
    }
    if (rotate(&source_image, angle) != ROTATION_OK)
        return error("Error rotating image.");
    enum write_status status = write_bmp(output_file_path, &source_image);
    free_image(&source_image);
    switch (status) {
    case (WRITE_HEADER_FAILED):
        return error("Write header error.");
    case (WRITE_ERROR):
        return error("Write error.");
    case (WRITE_FILE_OPEN_ERROR):
        return error("Error open file.");
    case (WRITE_OK):
        return 0;
    }
}
