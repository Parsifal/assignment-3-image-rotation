#include "transformations.h"

/* создаёт копию изображения, которая повёрнута на 90 градусов */
enum rotation_status rotate90(struct image *source) {
    struct image new_image;
    if (!create_image(&new_image, source->height, source->width)) return ROTATION_OUT_OF_MEMORY;
    for (uint64_t i = 0; i < source->height; i++){
        for (uint64_t j = 0; j < source->width; j++){
            new_image.data[(source->width - j - 1) * source->height + i] = source->data[i * source->width + j];
        }
    }
    free_image(source);
    *source = new_image;
    return ROTATION_OK;
}

/* создаёт копию изображения, которая повёрнута на 180 градусов */
enum rotation_status rotate180(struct image *source) {
    struct image new_image;
    if (!create_image(&new_image, source->width, source->height)) return ROTATION_OUT_OF_MEMORY;
    for (uint64_t i = 0; i < source->height; i++){
        for (uint64_t j = 0; j < source->width; j++){
            new_image.data[i * source->width + j] = source->data[(source->height - i) * source->width - j - 1];
        }
    }
    free_image(source);
    *source = new_image;
    return ROTATION_OK;
}
/* создаёт копию изображения, которая повёрнута на 270 градусов */
enum rotation_status rotate270(struct image *source) {
    struct image new_image;
    if (!create_image(&new_image, source->height, source->width)) return ROTATION_OUT_OF_MEMORY;
    for (uint64_t i = 0; i < source->height; i++) {
        for (uint64_t j = 0; j < source->width; j++) {
            new_image.data[(j + 1) * source->height - i - 1] = source->data[i * source->width + j];
        }
    }
    free_image(source);
    *source = new_image;
    return ROTATION_OK;
}

enum rotation_status rotate(struct image* source, const int64_t angle) {
    switch (angle) {
    case 90:
    case -270:
        return rotate90(source);
    case 180:
    case -180:
        return rotate180(source);
    case 270:
    case -90:
        return rotate270(source);
    }
    return ROTATION_OK;
}
